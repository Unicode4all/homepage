class ArticlesController < ApplicationController
    def show
	@article = Article.find(params[:id])
    end

    def new
	@article = Article.new
    end

    def index
    	@page_title = "Blog"
	@articles = Article.paginate(:page => params[:page], :per_page => 3)
    end

def edit
  @article = Article.find(params[:id])
end

def destroy
  @article = Article.find(params[:id])
  @article.destroy

  redirect_to articles_path
end

def update
  @article = Article.find(params[:id])

  if @article.update(article_params)
    redirect_to @article
  else
    render 'edit'
  end
end

    def create
    	uploaded_io = params[:article][:image]
  	File.open(Rails.root.join('public', 'uploads', uploaded_io.original_filename), 'wb') do |file|
    	file.write(uploaded_io.read)

  end

  	@article = Article.new(article_params)
	@article.image = "/uploads/#{uploaded_io.original_filename}"
  @article.save
  redirect_to @article
    end
end

private
def article_params
params.require(:article).permit(:title, :image, :overview, :text)
end
