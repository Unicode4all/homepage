Rails.application.routes.draw do
  devise_for :users

   resources :articles, :path => "blog" do
   resources :comments

   end

  get 'users/profile', as: 'user_url'
  root 'welcome#index'

end
